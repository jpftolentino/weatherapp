const express = require('express');
const fetch = require('node-fetch');
const crg = require('city-reverse-geocoder');
const bodyParser = require('body-parser');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/weather', (req, res) => {
  const url = 'https://api.darksky.net/forecast'
  fetch(`${url}/${process.env.DARKSKY_API_KEY}/43.6529,-79.3849?units=si`)
    .then(response => response.json())
    .then(json => {
      res.send({express: json});  
    });
});

app.get('/city', (req, res) => {
  const city = crg(43.7506,-79.4435, 3);
  res.send({express: city[0]});
});

app.listen(port, () => console.log(`Listening on port ${port}`));