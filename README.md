# Push Weather Application Submission

Create a simple web app that uses a weather API (e.g. https://darksky.net/dev) to retrieve the current weather for Toronto. This includes temperature and status (e.g. raining, cloudy, etc.). There is no user input, there should just be the weather for Toronto displayed on the screen in some way. You are not required to create a loader or anything, you only need to make the API request when the page loads and display the data when it is retrieved. It is up to you how to display the information to the user.


## Summary

Front-End: React <br />
Back-End: Express (Acts as proxy server to make api calls to end points)

## Instructions

Clone the repository or download as zip

```
git clone git@gitlab.com:jpftolentino/weatherapp.git
```

Install nodemon if not downloaded already

```
sudo npm i nodemon -g
```

Change directory into weatherapp

```
cd weatherapp
```

Initialize the repository and install all dependencies for server

```
npm init
```

Initialize the repository and install all dependencies for client

```
cd client
```

```
yarn
```

rebuild the project

```
cd ..
```

```
yarn
```

To compile and start the project

```
sudo yarn dev
```

Copy and paste url below to see the project

```
localhost:3000
```

### `.env`

Pushed my Darksky api key so reviewer doesn't have to go on website and get their own api key