import React from 'react'
import Skycons from 'react-skycons'

class Skyicons extends React.Component {  

  render () {
    return (
      <Skycons 
        width="128"
        height="128"
        color='black'
        icon={this.props.icon} 
        autoplay={true}
      />
    )
  }
}

export default Skyicons;