import React, { Component } from 'react';
import './App.css';
import Skyicons from './icons/Skyicon';
import celciusicon from './Degrees-Celcius.svg';

class App extends Component {

  constructor(props){
    super(props);

    var days = ["Sunday","Monday", "Tuesday", "Wednesday","Thursday", "Friday", "Saturday"];
    var months = ["January","February","March","April","May","June","July",
    "August","September","October","November","December"];

    var today = new Date();
    var dayofweek = days[today.getDay()];
    var realdate = months[today.getMonth()] + ' ' + today.getDate() + ' ' + today.getFullYear();

    this.state = {
      responseCurrently: '',
      city: '',
      dayofweek: dayofweek,
      realdate: realdate,
    };
  }

  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ responseCurrently: res.express.currently }))
      .catch(err => console.log(err));

    this.getCity()
      .then(res => this.setState({ city: res.express.city }))
      .catch(err => console.log(err)); 
  }
  
  callApi = async () => {
    const response = await fetch('/weather');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  getCity = async () => {
    const response = await fetch('/city');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };


  render() {

    var city = this.state.city;
    var temperature = Math.round(this.state.responseCurrently.temperature);
    var precipProb = Math.round(this.state.responseCurrently.precipProbability) * 100 + '%';
    var humidity = this.state.responseCurrently.humidity * 100 + '%';
    var windSpeed = this.state.responseCurrently.windSpeed + ' km/h'
    var summary = this.state.responseCurrently.summary;

    if(this.state.responseCurrently.icon){
      var icon = this.state.responseCurrently.icon.toUpperCase().replace(/ /g,"_").replace(/-/g,"_");
    }


    return (
      <div class="card">
        
        <div>
          <span class="day-of-week">{this.state.dayofweek}</span> 
          <span class="real-date">{this.state.realdate}</span>
        </div>
        
        <p class="city-text">{city}</p>
        
        <div>
          <div class="temperature">
            <div class="weather-icon"><Skyicons icon={icon}/></div>
            <div class="temperature-text">{temperature}</div>
            <img src={celciusicon} alt="Celcius"/>
          </div>
          
          <div class="additional-info">
            <p>Precipitation: <span class="precip-prob-text">{precipProb}</span></p>
            <p>Humidity: <span class="humidity-text">{humidity}</span></p>
            <p>Wind: <span class="windspeed-text">{windSpeed}</span></p>
          </div>
        
        </div>
        <br/>
      
        
        <div class="summary"><span>{summary}</span></div>
      </div>    

    );
  }
}

export default App;
